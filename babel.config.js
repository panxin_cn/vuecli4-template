const plugins = ["@vue/babel-plugin-transform-vue-jsx"];
// 生产环境移除console
if (process.env.NODE_ENV === "production") {
  plugins.push("transform-remove-console");
}
module.exports = {
  plugins: plugins,
  presets: [
    "@vue/cli-plugin-babel/preset",
    [
      "@babel/preset-env",
      //浏览器兼容性问题解决
      {
        useBuiltIns: "usage",
        corejs: 3 // <---  此处加个这个，就没有报错警告了
      }
    ]
  ]
};
